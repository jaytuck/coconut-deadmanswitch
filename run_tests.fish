#!/usr/bin/env fish

# See https://stackoverflow.com/questions/54895002/modulenotfounderror-with-pytest for why we use PYTHONPATH=.
PYTHONPATH=. pytest
