import os
import toml
import arrow
import sqlite3
import flask
import socket
import importlib.resources
from werkzeug.middleware.proxy_fix import ProxyFix
from functools import wraps
from dataclasses import dataclass


# Load config
conf = toml.loads(importlib.resources.read_text('dms', 'default_config.toml'))
if "DMS_CONFIG_FILE" in os.environ:
    conf.update(toml.load(os.environ.get("DMS_CONFIG_FILE")))


@dataclass
class Monitor:
    name: str
    group: str
    lastupdate: arrow.arrow.Arrow
    validtime: int
    success: bool = True
    message: str = ""
  

def setup_db(conn):
    # This will create the required table if not already there
    conn.execute("""
        CREATE TABLE IF NOT EXISTS "monitors" (
        "checkname" TEXT,
        "checkgroup" TEXT,
        "lastupdate" INTEGER,
        "validtime" INTEGER,
        "checksuccess" BOOLEAN,
        "checkmessage" TEXT
    )""")
    # Uniquify the name+group
    conn.execute("""
    CREATE UNIQUE INDEX IF NOT EXISTS "name_group_unique_index" on "monitors" (
        "checkname", "checkgroup"
    )""")

    # Make sure we are in WAL mode
    conn.execute('PRAGMA journal_mode=WAL;')
    conn.commit()


def prep_for_db(mon):
    return (
        mon.name,
        mon.group,
        mon.lastupdate.timestamp(),
        mon.validtime,
        mon.success,
        mon.message,
    )


def convert_from_db(row):
    return Monitor(
        name=row[0],
        group=row[1],
        lastupdate=arrow.get(row[2]).to(conf['timezone']),
        validtime=row[3],
        success=bool(row[4]),
        message=row[5]
    )


## Useful DB functions
def update_monitor(dbconn, monitor):
    dbconn.execute('INSERT OR REPLACE into monitors values (?,?,?,?,?,?)', prep_for_db(monitor))
    dbconn.commit()

def delete_monitor(dbconn, name, group):
    dbconn.execute('DELETE FROM monitors WHERE checkname=? AND checkgroup=?', (name, group))
    dbconn.commit()

def get_all_monitors(dbconn):
    return map(
        lambda monitor: convert_from_db(monitor),
        dbconn.execute('select * from monitors')
    )


def get_monitor_group(dbconn, group):
    return map(
        lambda monitor: convert_from_db(monitor),
        dbconn.execute('select * from monitors where checkgroup=?', (group,))
    )


def get_groups(dbconn):
    return dbconn.execute('select distinct checkgroup from monitors')


def failed_monitor(monitor):
    return (
        monitor.success == False or
        (arrow.now() - monitor.lastupdate).total_seconds() > monitor.validtime
    )


def check_failure_txt(txt):
    '''This is used with a request, to convert various different failure texts to a bool'''
    return (txt == False or txt.lower() == 'false' or txt == '0' or txt.lower() == 'no')

app = flask.Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_host=1)  # App is behind one nginx proxy that sets the -For and -Host headers.
# Add the conf configs into the flask config
app.config.update(**conf)

def db(func):
    '''This decorator will connect to the db at the start of a request and close after'''
    @wraps(func)
    def wrapper(*args, **kwargs):
        dbconn = sqlite3.connect(app.config['dbfile'])
        try:
            return func(dbconn, *args, **kwargs)
        finally:
            dbconn.close()
    return wrapper


@app.route('/')
@db
def index(dbconn):
    table = monitor_table()
    return flask.render_template('index.html', monitor_table=table)


@app.route('/monitor_table')
@db
def monitor_table(dbconn):
    return flask.render_template('table.html', failed_monitor=failed_monitor, existing_monitors=get_all_monitors(dbconn))


@app.route('/monitor_builder')
def monitor_builder():
    group = flask.request.args["groupname"]
    name = flask.request.args["monitorname"]
    if time := flask.request.args.get('monitortime'):
        time = int(time) * 60 * 60
    else:
        time = None
    if not (errormessage := flask.request.args.get('errormessage')):
        errormessage = 'failure because bloop borked'

    return flask.render_template('monitor_builder.html', group=group, name=name, time=time, errormessage=errormessage)


@app.route('/groups')
@db
def groups(dbconn):
    return flask.jsonify(list(get_groups(dbconn)))


@app.route('/status/<monitor_group>')
@db
def monitor_group(dbconn, monitor_group):
    monitors = list(filter(failed_monitor, get_monitor_group(dbconn, monitor_group)))
    if monitors:
        t = map(
            lambda m: f'monitor:{m.name} - lastupdate:{m.lastupdate} - {m.message}',
            monitors
        )
        resp = flask.Response('\n'.join(t), 500)
        resp.headers['Content-Type'] = 'text/plain'
        return resp
    else:
        return ('', 200)


@app.route("/update/<monitor_group>/<monitor_name>", methods=['GET','POST'])
@db
def update_monitor_endpoint(dbconn, monitor_group, monitor_name):
    now = arrow.now()

    if flask.request.is_json:
        j = flask.request.get_json()
    else:
        j = {}

    try:
        if 'validtime' in flask.request.args:
            validtime = int(flask.request.args['validtime'])
        elif 'validtime' in j:
            validtime = int(j['validtime'])
        else:
            validtime = 93600  # 26 hrs
    except:
        return ('validtime must be provided as an int of seconds that the monitor is valid for', 500)

    if 'success' in flask.request.args:
        failed = check_failure_txt(flask.request.args['success'])
        success = not failed
    elif 'success' in j:
        success = bool(j['success'])
    else:
        success = True

    if 'message' in flask.request.args:
        message = str(flask.request.args['message'])
    elif 'message' in j:
        message = str(j['message'])
    else:
        message = ''

    try:
        remotename = socket.gethostbyaddr(flask.request.remote_addr)[0]
    except:
        remotename = 'unknown'
    message = f'from - {flask.request.remote_addr}({remotename}): {message}'

    print(f'got update for {monitor_name} at {now} with success {success}{" and message " if message else ""}{message}')
    m = Monitor(monitor_name, monitor_group, now, validtime, success, message)
    update_monitor(dbconn, m)
    return 'updated!'


@app.route("/delete/<monitor_group>/<monitor_name>", methods=['GET','POST'])
@db
def delete_monitor_endpoint(dbconn, monitor_group, monitor_name):
    delete_monitor(dbconn, monitor_name, monitor_group)
    return flask.Response(
            'deleted!',
            headers={'HX-Trigger':'refreshTable'}, # Refresh the table when deleted - https://htmx.org/headers/hx-trigger/
    )


def getapp(testconfig={}):
    app.config.update(**testconfig)
    dbconn = sqlite3.connect(app.config['dbfile'])
    setup_db(dbconn)
    dbconn.close()
    return app

