#!/usr/bin/env fish

# not currently using coconut
#coconut --target 3.8 --watch . &
set -x FLASK_APP dms:getapp
set -x FLASK_ENV development
flask run --host 0.0.0.0 --port 8009
