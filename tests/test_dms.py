import os
import dms
import json
import arrow
import pytest
import tempfile
import sqlite3


@pytest.fixture
def app():
    db_fd, db_path = tempfile.mkstemp()
    try:
        app = dms.getapp(testconfig={
                'dbfile': db_path,
            })

        yield app
    finally:
        os.close(db_fd)
        os.unlink(db_path)

@pytest.fixture
def client(app):
    with app.app_context():
        dbconn = sqlite3.connect(app.config['dbfile'])
        # make dummy data
        ts = arrow.now()
        for m, g in (
                    ('test1','grp1'),
                    ('test2','grp1'),
                    ('test3','grp2')
                ):
            mon = dms.Monitor(
                    name=m,
                    group=g,
                    lastupdate=ts,
                    validtime=(24*60*60),
                )
            dms.update_monitor(dbconn, mon)
        for m, g in (
                    ('failed1','grp2'),
                    ('failed2','grp3'),
                ):
            mon = dms.Monitor(
                    name=m,
                    group=g,
                    lastupdate=ts.shift(hours=-48),
                    validtime=(24*60*60),
                )
            dms.update_monitor(dbconn, mon)
    dbconn.close()
    return app.test_client()


def test_db_conversions():
    testtime = arrow.get('2021-06-15')
    monitor1 = dms.Monitor(
            name='test',
            group='testgrp',
            lastupdate=testtime,
            validtime=testtime.timestamp(),
        )
    assert dms.prep_for_db(monitor1) == ('test', 'testgrp', testtime.timestamp(), testtime.timestamp(), True, "")
    assert dms.convert_from_db(('test', 'testgrp', testtime.timestamp(), testtime.timestamp(), True, "")) == monitor1
    
    assert monitor1 == dms.convert_from_db(dms.prep_for_db(monitor1))


def test_db(client):
    response = client.get('/')
    assert response.status_code == 200
    assert b'test1' in response.data
    assert b'failed1' in response.data
    response = client.get('/status/grp1')
    assert response.status_code == 200  # no bad monitors
    assert response.data == b''
    response = client.get('/status/grp2')
    assert response.status_code == 500  # bad monitors
    assert b'failed1' in response.data
    response = client.get('/status/grp3')
    assert response.status_code == 500  # bad monitors
    assert b'failed2' in response.data

    response = client.get('/groups')
    assert response.status_code == 200
    j = json.loads(response.data)
    assert len(j) == 3

    response = client.get('/update/newgrp/newmon')
    assert response.status_code == 200
    response = client.get('/')
    assert b'newgrp' in response.data
    assert b'newmon' in response.data

    response = client.get('/update/newgrp2/newmon2?validtime=654&success=False&message=borkified')
    assert response.status_code == 200
    response = client.get('/')
    assert b'654' in response.data
    assert b'borkified' in response.data
    
    response = client.get('/delete/newgrp2/newmon2')
    assert response.status_code == 200
    response = client.get('/')
    assert b'borkified' not in response.data

