# coconut-deadmanswitch

This project is a Dead Man's Switch monitoring tool.

It was originally written in Coconut, the functional extension to Python - see http://coconut-lang.org/ - however I was not getting enough extra functionality from coconut so over time I have just removed it. I may re-add it in the future if there's a good reason to.

The way it works is:
* It listens for monitor updates
* If it sees a monitor update it looks in the database (a sqlite instance) and:
  * If it finds an existing monitor with the same name it replaces it with the update
  * If it doesn't find an existing monitor it creates one
* It exposes the monitor statuses on a URL, which you can query to check if any monitors haven't responded in the expected time

This project has the features:
* It is very simple and lightweight
* If you delete the database it will be repopulated by the checks running


# Database
There is only one table in the DB, and there are these columns in the DB:

column|type|what it represents
-|-|-
checkname|text|the name of the check, which is also it's ID
checkgroup|text|the group of the check, to allow querying the status of group of checks at once
lastupdate|int|the last update time in seconds from the epoch
validtime|int|the amount of seconds the check is valid for
checksuccess|int|whether the check has been actively marked as failed 1=success, 0=fail to match pythonic truthy
checkmessage|text|an optional message from the check result


# How to use:
Start up the docker container, and go to the url it is running on - this page has a bunch of tips on how to use the tool, as well as a list of the monitors in the DB and their status.

# Container build notes
Run on fedora server v33
```
podman build --pull -f container/Dockerfile . -t docker.io/jaytuckey/coconut-deadmanswitch:v1.2.1 -t docker.io/jaytuckey/coconut-deadmanswitch:latest

podman push docker.io/jaytuckey/coconut-deadmanswitch:latest
podman push docker.io/jaytuckey/coconut-deadmanswitch:v1.0.0
```

# Update steps

* Update poetry dependencies
* Update JS libraries (htmx)
* Run tests
* Tag a new version
* rebuild docker image



